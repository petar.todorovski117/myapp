<?php echo validation_errors(); ?>

<?php echo form_open('news/update'); ?>
<input type="hidden" name="id" value="<?php echo $news_item['id']; ?>">

<br>
<h4><?php echo $news_item['title']?></h4>
<p class="card-text"><small class="text-muted">Posted on: <?php echo $news_item['created'] ?></small></p><br>
<p><?php echo $news_item['text']?></p>


<div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="title">Title</label>
      <input type="input" class="form-control" name="title" value="<?php echo $news_item['title'];?>" required="" /><br />
  </div>
</div> 
<div class="form-row">
	<div class="col-md-6 mb-3">
      <div class="form-group">
          <label for="text">Text</label>
          <textarea id="editor1" class="form-control" name="text" rows="5"  required=""><?php echo $news_item['text'];?></textarea><br />
      </div>
  </div>
</div>

<input type="submit"  class="btn btn-primary" id="btn" name="submit" value="Save changes"  />


</form>
