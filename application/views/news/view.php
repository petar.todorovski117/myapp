

<br>
<div class="card-header">
	<h4><?php echo $news_item['title']?>
		<a onclick="return confirm('Are you sure you want to delete?');" href="<?php echo site_url('news/delete/'.$news_item['slug']); ?>">
			<button id="right" class="btn btn-outline-primary">Delete</button>
		</a>
	</h4>
</div>
    <p class="card-text"><small class="text-muted">Posted on: <?php echo $news_item['created'] ?></small></p><br>
<p><?php echo $news_item['text']?></p>

<hr>


<a href="<?php echo site_url('news/edit/'.$news_item['slug']); ?>">
<button  id="center" class="btn btn-outline-primary btn-lg">Edit</button>
</a>