<div class="content">
<?php foreach ($news as $news_item): ?>

<div class="card">
  <div class="card-header">
    Featured
  </div>
  <div class="card-body">
    <h4 class="card-title"><?php echo $news_item['title']; ?>
    	<div id="right" class="dropdown">
 			  <button type="button" class="btn btn-outline-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>

 			 	 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    				<a class="dropdown-item" href="<?php echo site_url('news/edit/'.$news_item['slug']); ?>"> Edit post</a>
            <a class="dropdown-item" href="<?php echo site_url('news/delete/'.$news_item['slug']); ?>"> Delete post</a>

  				</div>
	   </div>
    </h4>
    <p class="card-text"><small class="text-muted">Posted on: <?php echo $news_item['created'] ?></small></p><br>


    <p class="card-text"><?php echo word_limiter($news_item['text'], 60); ?></p>

    <a class="btn btn-primary" href="<?php echo site_url('news/'.$news_item['slug']); ?>"  role="button">Read more</a>
  </div>
</div>
<br>

<?php endforeach; ?></div>


 