 <html>
 <head>
  <meta charset="utf-8"/>
  <title>myApp</title>
  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~89181109/sis_tmp3/CodeIgniter//assets/css/style.css">
 <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="<?php echo site_url('home'); ?>">myApp</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>  


      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('home'); ?>">Home</a>
            <span class="sr-only ">(current)</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('about'); ?>">About</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('news'); ?>">News Feed</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('news/create'); ?>">Create News</a>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('user_authentication/signup'); ?>"><i class="fas fa-user-plus"></i> Signup</a>
        </li> 
        <li class="nav-item">
         <a class="nav-link" href="<?php echo site_url('user_authentication/signin'); ?>"><i class="fas fa-user"></i> Login</a>
       </li>
     </ul>
   </div>
 </div>

</nav>
<div class="container">
  <hr>
  <h3><?php if(isset($title)) echo $title; ?></h3> 


</div>
<div class="container">

<!--     
  

       <a href="<?php echo site_url('news'); ?>">News</a><br>
       <a href="<?php echo site_url('news/create'); ?>">Create News</a><br>

       <a href="<?php echo site_url('user_authentication/admin'); ?>">Admin</a><br> --> 
