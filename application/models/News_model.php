<?php
class News_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function get_news(){
    $this->db->order_by('id','DESC');
    $query = $this->db->get('news');
    return $query->result_array();
  }

  public function get_news_where($slug){

    $query = $this->db->get_where('news', array('slug' => $slug));
    return $query->row_array();

  }
  public function set_news(){

    $slug = url_title($this->input->post('title'), 'dash', TRUE);
    $data = array('title' => $this->input->post('title'),
      'slug' => $slug,
      'text' => $this->input->post('text'));
    /**/
    return $this->db->insert('news',$data);
  }

  public function save_news($slug){


    $data = array(
      'title' => $this->input->post('title'),
      'slug' => $this->input->post('slug'),
      'text' => $this->input->post('text')
    );

    $this->db->set($data);
    $this->db->where('slug',$this->input->post('slug'));
    return $this->db->update('news');
  }


  public function delete_news($slug){
    $this->db->where('slug',$slug);
    return $this->db->delete('news');
  }

  public function update_news(){

    $slug = url_title($this->input->post('title'));

    $data = array('title' => $this->input->post('title'),
      'slug' => $slug,
      'text' => $this->input->post('text'));
    $this->db->where('id',$this->input->post('id'));

    return $this->db->update('news',$data);
  }




}