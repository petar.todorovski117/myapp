<?php
class News extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model');
        $this->load->helper('url_helper');
    }

    public function create(){
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('text', 'Text', 'required');
        $data['title'] = "Create new post";

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header', $data);
            $this->load->view('news/create');
            $this->load->view('templates/footer'); 
        }
        else{
            $this->news_model->set_news();
            redirect('news');
        }
        
    }

    public function edit($slug = NULL){
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('text', 'Text', 'required');
        
        $data['title'] = "Edit News";
        $data['news_item'] = $this->news_model->get_news_where($slug);

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header', $data);
            $this->load->view('news/edit');
            $this->load->view('templates/footer'); 
        }
        else{
            $this->news_model->set_news();
            redirect('news');
        }
        
    }
    public function update(){
        $this->news_model->update_news();
                    redirect('news');

    }

    public function index()
    {
        $data['news'] = $this->news_model->get_news();
        $data['title'] = 'News feed';

        $this->load->view('templates/header', $data);
        $this->load->view('news/index', $data);
        $this->load->view('templates/footer');
    }

    public function view($slug = NULL)
    {
        $data['news_item'] = $this->news_model->get_news_where($slug);
        
        $data['title'] = "News Item";
        $this->load->view('templates/header', $data);
        $this->load->view('news/view', $data);
        $this->load->view('templates/footer');

    }

    public function delete($slug){
        $this->news_model->delete_news($slug);
        redirect('news');
    }
    

}
